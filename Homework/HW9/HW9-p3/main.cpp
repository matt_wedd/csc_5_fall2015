/*
* Name: Harley Webb
* Student ID: 2511318
* Date: 12/05/15
* HW: 9
* Problem: 3
* I Harley Webb, certify this is my own work and code.
*/
#include <iostream>
#include <cstdlib>
#include <vector>
using namespace std;

int main(int argc, char** argv) {
    int num = 1, count=0;
    vector<int>v;
    do{
        cout<< "Enter values you wish to store in the array";
        cout <<" and enter -1 when you wish to stop:";
        cin >> num;
        v.push_back(num);
        count++;
      }while(num != -1);
      int*p = new int[count];
      for(int i=0;i<count;i++)
      {
          p[i]=v[i];
          cout <<p[i]<<" ";
      }
      delete []p;
      
    return 0;
}

