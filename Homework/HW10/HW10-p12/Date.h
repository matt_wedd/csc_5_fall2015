/* 
 * File:   Date.h
 * Author: vashwebb
 *
 * Created on December 10, 2015, 5:57 PM
 */

#ifndef DATE_H
#define	DATE_H
#include "time.h"
class Date: public Time {

    private:
    int month;
    int year;
    
public:
    Date();
    Date(int, int);
    Date(int, int , int ,int,int);
    void output(std::ostream&);
    int getMonth() const;
    int getYear() const;
    void setMonth(int);
    void setYear(int);

    
};

#endif	/* DATE_H */

