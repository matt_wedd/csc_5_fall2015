/*
* Name: Harley Webb
* Student ID: 2511318
* Date: 11/14/15
* HW: 8
* Problem: 1
* I Harley Webb, certify this is my own work and code.
*/
#include <vector>
#include <cstdlib>
#include <iostream>
using namespace std;

void chanVec(vector<int> &v, int loc);

int main(int argc, char** argv) {

    cout <<"Enter a location number you wish to"
            <<" to be deleted from the vector:";
    int num=0;
    cin >> num;
    vector<int> v;
    
    for (int i=0;i<7;i++)
        v.push_back(i*2);
    
    chanVec(v,num);
    
   
    return 0;
}

void chanVec(vector<int> &v, int loc)
{
    if(v.size() != 0)
    {
        for(int i=0;i<v.size();i++)
        {
            if(i == loc)
            {
                swap(v[loc-1], v[loc]);
                loc++;
            }
        }
        v.pop_back();
        
        for(int i=0;i<v.size();i++)
            cout<<v[i]<<" ";
    }    
    else 
    {
    cout <<"Invalid input";
    }
}