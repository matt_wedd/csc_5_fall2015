/* 
 * File:   Time.h
 * Author: vashwebb
 *
 * Created on December 10, 2015, 5:57 PM
 */

#ifndef TIME_H
#define	TIME_H
#include <fstream>
#include <iostream>
#include <cstdlib>

class Time
{
private:
    int second;
    int minute;
    int hour;
public:
    Time();
    Time(int, int, int);
    void output(std::ostream&);
    int getSecond() const;
    int getMinute() const;
    int getHour() const;
    void setSecond(int);
    void setMinute(int);
    void setHour(int);
    
    
   
};


#endif	/* TIME_H */

