/*
* Name: Harley Webb
* Student ID: 2511318
* Date: 10/31/15
* HW: 7
* Problem: 3
* I Harley Webb, certify this is my own work and code.
*/

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
using namespace std;

int main(int argc, char** argv) {

    ifstream infile;
    int sum=0,n=0,m=0;
    infile.open("data1.dat");
    
    while (infile >> n)
    {
        if(n % 2 == 0)
        {
            sum = sum +n;
        }
    }
    infile.close();
    
    infile.open("data2.dat");
    
    while (infile >> n)
    {
        if(n % 2 == 0)
        {
            sum = sum +n;
        }
    }
    infile.close();
    
    
    infile.open("data3.dat");
    
    while (infile >> n)
    {
        if(n % 2 == 0)
        {
            sum = sum +n;
        }
    }
    infile.close();
    
    cout <<"The sum of all even numbers is "<<sum<<endl;
    
    
    return 0;
}

