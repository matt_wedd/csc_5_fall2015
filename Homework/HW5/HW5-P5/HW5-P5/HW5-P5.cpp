/*
* Name: Harley Webb
* Student ID: 2511318
* Date: 10/07/15
* HW: 5
* Problem: 5
* I Harley Webb, certify this is my own work and code.
*/

#include "stdafx.h"
#include <iostream>
#include <iomanip>
using namespace std;


const double lit_per_gal = 0.264179;

double function(double miles1, double liters2)
{

	return miles1 / (lit_per_gal*liters2);
}


int _tmain(int argc, _TCHAR* argv[])
{
	char quit;
	do{
		double miles, liters;
		cout << "How many liters of gas did your car"
			<< " consume? ";
		cin >> liters;

		cout << "How many miles did you drive? ";
		cin >> miles;

		cout << "Your car consumes " << function(miles, liters)
			<< " miles per gallon." << endl;

		cout << "If you would like to do this again enter y for "
			<< "yes or n for no. ";

		cin >> quit;

	} while (quit == 'y' || quit == 'Y');

	return 0;
}

