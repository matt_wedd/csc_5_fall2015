//
// Name: Harley Webb
// Student ID : 2511318
// Date : 10 / 01 / 15
// HW : 4
// Problem : 4
// I Harley Webb certify this is my own work and code
//

#include "stdafx.h"
#include <iostream>
#include <iomanip>
using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	double counter = 0, exer_num = 0, scor_num = 0,
		total_score = 0, add_scor = 0, add_tot = 0,
		one_hund=100;
	cout << "How many exercises to input? ";
	cin >> exer_num;
	cout << endl;;
	do{
		counter++;
		cout << "Score received for exercise "
			<< counter << ": ";
		cin >> scor_num;

		cout << "Total points possible for "
			<< "exercise " << counter << ": ";
		cin >> total_score;
		cout << endl;
		add_scor = add_scor + scor_num;
		add_tot = add_tot + total_score;
		

	} while (exer_num != counter);
	cout << "Your total is "
		<< add_scor << " out of "
		<< add_tot << ", or "<< setprecision(2)
		<< fixed << showpoint
		<< add_scor / add_tot *one_hund
		<< "%"<<endl;


	return 0;
}

