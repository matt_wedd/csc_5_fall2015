
// This program demonstrates the logic || operator

#include "stdafx.h"
#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	const double aPlus = 4.0, aGrade = 4.0, aMinus = 3.7,
		bPlus = 3.3, bGrade = 3.0, bMinus = 2.7,
		cPlus = 2.3, cGrade = 2.0, cMinus = 1.7,
		dPlus = 1.3, dGrade = 1.0, dMinus = 0.7,
		fGrade = 0;
     
	cout << "Please enter your grade letter: ";
	double gradeNum;
	cin >> gradeNum;
	cout << "\n";

	if (gradeNum > aPlus)
		cout << "You got an A+ ";
	else if (gradeNum == aGrade)
		cout << "You got an A";
	else if (gradeNum >= aMinus)
		cout << "You got an A-";
	else if (gradeNum >= bPlus)
		cout << "You got a B+ ";
	else if (gradeNum >= bGrade)
		cout << "You got a B ";
	else if (gradeNum >= bMinus)
		cout << "You got a B- ";
	else if (gradeNum >= cPlus)
		cout << "You got a C+ ";
	else if (gradeNum >= cGrade)
		cout << "You got a C ";
	else if (gradeNum >= cMinus)
		cout << "You got a C-";
	else if (gradeNum >= dPlus)
		cout << "You got a D+ ";
	else if (gradeNum >= dGrade)
		cout << "You got a D ";
	else if (gradeNum >= dMinus)
		cout << "You got a D- ";
	else
		cout << "You got an F ";

	return 0;
}

