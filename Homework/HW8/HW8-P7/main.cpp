/*
* Name: Harley Webb
* Student ID: 2511318
* Date: 11/20/15
* HW: 8
* Problem: 7
* I Harley Webb, certify this is my own work and code.
*/

#include <iostream>
#include <cstdlib>
#include <vector>
using namespace std;

bool driverTest(vector<int>v,int num);

int main(int argc, char** argv) {

    int num2=0;
    
    do{
        cout<<"Enter a number you wish to input into a vector:";
                int num=0;
        cin >> num;
        vector<int>v;
        for(int i=0;i<10;i++)
            v.push_back(3*i);
    
        if(driverTest(v,num) == true)
          cout<<"The number is currently being used and cannot"
                  " be put inside the vector"<<endl;
        else
        {
            v.push_back(num);
            cout<<"The number has been added to the vector."<<endl;
        }
        cout<<endl<<"1. Try again"<<endl<<"2. Quit"<<endl<<"enter either number"
                <<" from the menu the option of your choice:";
      
        cin >> num2;
      }while(num2 != 2);
    
    return 0;
}
    
bool driverTest(vector<int>v, int num)
{
 for(int i=0;i<v.size();i++)
    {
     if(v[i] == num)
         return true; 
    }    
 return false;

}
