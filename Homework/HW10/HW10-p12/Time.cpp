/* 
 * File:   Time.cpp
 * Author: vashwebb
 * 
 * Created on December 10, 2015, 5:57 PM
 */

#include "Time.h"

    Time::Time()
{
    hour = 1;
    minute = 0;
    second = 0;
}



Time::Time(int hours,int second , int mintues)
{
   this->hour = hours;
   this->second = second;
   this->minute = mintues;
}
    
    int Time::getHour() const {
        return hour;
    }

    void Time::setHour(int hour) {
        this->hour = hour;
    }

    int Time::getMinute() const {
        return minute;
    }

    void Time::setMinute(int minute) {
        this->minute = minute;
    }

    int Time::getSecond() const {
        return second;
    }

    void Time::setSecond(int second) {
        this->second = second;
    }

void Time::output(std::ostream& out)
{
    out<<"Hours:"<<hour;
    out<<std::endl<<"Minutes"<<minute;
    out<<std::endl<<"Seconds:"<<second;
}
