/*
* Name: Harley Webb
* Student ID: 2511318
* Date: 11/15/15
* HW: 8
* Problem: 5
* I Harley Webb, certify this is my own work and code.
*/

#include <iostream>
#include <cstdlib>
using namespace std;

void arChan(int array[],int num);

int main(int argc, char** argv) {

    int cap=10;
    int array[cap]={0,2,4,6,8,10,12,14,16,18};
    
    cout <<"(0,2,4,6,8,10,12,14,16,18)";
    cout <<endl<<endl;
    cout <<"Choose a number from the list"
         <<" you wish to be deleted:";
    int num=0;
    cin >> num;
    
    arChan(array, num);
    return 0;
}

void arChan(int array[], int num)
{
    for(int i=0;i<10;i++)
    {
        if(array[i]==num)
        {
            array[i]=array[i+1];
            num++;
            num++;
        }
    }
}