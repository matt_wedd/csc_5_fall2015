/*
* Name: Harley Webb
* Student ID: 2511318
* Date: 11/06/15
* HW: 7
* Problem: 4
* I Harley Webb, certify this is my own work and code.
*/

#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <cstdlib>
using namespace std;

int main(int argc, char** argv) {

    ifstream infile;
    double num=0,sum=0;
    double counter=0;
    vector<double> v;
    
    infile.open("median.dat");
    while (infile >> num )
    {
        v.push_back(num);
        counter++;
    }
    infile.close();
    
    sum = (v.size()/ 2.)-1. ;
    double sum2 =sum +1.;
    double total=(v[sum]+v[sum2])/2.;
    
    cout << "The median of the file is "<<total<<"."<<endl;
    
    return 0;
}

