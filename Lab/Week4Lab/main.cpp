/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 15, 2015, 11:09 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

int main(int argc, char** argv) {

    // Prompt the user
    cout << "Please enter your test score";
    int testScore;
    
    // Get input user
    cin >> testScore;
    
    if(testScore > 50)
    {
        cout << "You got an A!";
        cout << "Keep it up!";
    }
    else
    {
        cout << "You got a F! ";
        cout << "Failed!";
    }
    
    return 0;
}

