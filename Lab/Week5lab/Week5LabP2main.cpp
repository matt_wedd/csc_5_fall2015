/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on October 1, 2015, 11:30 AM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;


int main(int argc, char** argv) {
    int num_picks=23, pic_take=0,four_num=4,
            three_num=3;
    
    do{
         cout << "Enter any amount 1-3 of tooth picks: ";
    cin >> pic_take;  
        
        if(num_picks > three || num_picks <1)
            {
                cout << "Please enter an amount of tooth picks"
                     << " less than three and more than one. \n";
                cout << "Enter any amount 1-3 of tooth picks: ";
                cin >> num_picks; 
            }
        else if (num_picks < four_num)
            {
                if(num_picks == 4)
                 num_picks=num_picks-3;
                else if (num_picks == 2){
                 num_picks=num_picks-2;}
                else if (num_picks == 1)
                 num_picks=num_picks-1;            
            }
         if(num_picks > 4)
          num_picks=num_picks-pic_take;
        
         cout << "There are "<< num_picks<<" left.\n";
    }while(num_picks >= 0);
    
    return 0;
}

