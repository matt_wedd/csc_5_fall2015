//
// Name: Harley Webb
// Student ID : 2511318
// Date : 9 / 30 / 15
// HW : 4
// Problem : 1
// I Harley Webb certify this is my own work and code
//

#include "stdafx.h"
#include <iostream>
#include <iomanip>
using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	int pos_num = 0, neg_num = 0, all_num = 0;
	int coun_pos = 0, coun_neg = 0;
	for (int i = 0; i < 10; i++)
	{

		cout << "Enter a number: ";
		int num_input;
		cin >> num_input;

		if (num_input > 0)
		{

			pos_num = pos_num + num_input;
			coun_pos++;
		}


		else if (num_input < 0)
		{
			neg_num = neg_num + num_input;
			coun_neg++;
		}

	}

	cout << "The sum of all postives is " << pos_num << endl;
	cout << "The sum of all non-postives is " << neg_num << endl;
	cout << "The average of all positives is " <<
		pos_num / coun_pos << endl;
	cout << "The average of all non-positives is" <<
		neg_num / coun_neg << endl;
	cout << "The sum of all numbers is " << pos_num
		+ neg_num << endl;

	return 0;
}

