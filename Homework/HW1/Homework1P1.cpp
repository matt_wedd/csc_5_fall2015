/*
* Name: Harley Webb	
* Student ID: 2511318
* Date: 9/11/15
* HW: 1
* Problem: 1
* I Harley Webb certify this is my own work and code
*/


#include "stdafx.h"
#include <iostream>
using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	int number_of_pods, peas_per_pod, total_peas;

	cout << "Press return after entering a number.\n";
	cout << "Enter the number of pods: \n";

	cin >> number_of_pods;

	cout << "Enter the numner of peas in a pod:\n";
	cin >> peas_per_pod;
	total_peas = number_of_pods * peas_per_pod;
	cout << "If you have ";
	cout << number_of_pods;
	cout << "pea pods\n";
	cout << "and";
	cout << peas_per_pod;
	cout << "peas in each pod, then\n";
	cout << "you have ";
	cout << total_peas;
	cout << " peas in all the pods.\n";

	return 0;
}

