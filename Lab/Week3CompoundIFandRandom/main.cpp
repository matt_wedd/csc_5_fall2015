/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 17, 2015, 10:51 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;


int main(int argc, char** argv) {

// Do not pass the 80 character mark
    // Avoid line wrapping
    
    // GUESSING GAME
    // Generate a random number
    srand(time(0)); // SEED
    int num = rand(); // Get a random number
    num %= 100; // Get a number between 0-99
    
    // PROMPT THE USER
    cout << "Please enter a number between 0-99: ";
    
    int userNum;
    cin >> userNum;
    
    if ( userNum == num)
    {
        cout << "Correct!" << endl;
    }
    else
    {
        cout << "Wrong!" << endl;
    }
    
    
    // Prompt the user for another number
    cout << "Please enter another number: " << endl;
    
    cin >> userNum;
    
    // Check if even or odd
    if (userNum % 2 == 0) // EVEN NUMBER
    {
        if (userNum > 0)
        {
            cout << "Positive and Even";
        }
        else
        {
            cout << "Negative and Even";
        }
    }
    else
    {
        if (userNum > 0)
        {
            cout << "Positive and Odd";
        }
        else
        {
            cout << "Negative and Odd";
        }
    }
    
    // METHOD 2 Compound boolean    
    cout << endl;
    
    if (userNum % 2 == 0 && userNum < 0)
    {
        cout << "Negative and Even";
    }
    else if (userNum % 2 == 0 && userNum > 0)
    {
        cout << "Positive and Even";
    }
    else if (userNum % 2 != 0 && userNum > 0)
    {
        cout << "Positive and Odd";
    }
    else
    {
        cout << "Negative and Odd";
    }
    
    return 0;
}
