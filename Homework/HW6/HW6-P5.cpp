/*
* Name: Harley Webb
* Student ID: 2511318
* Date: 10/17/15
* HW: 6
* Problem: 5
* I Harley Webb, certify this is my own work and code.
*/

#include <iostream>
#include <cstdlib>
using namespace std;



int main(int argc, char** argv) 
{
 
    int x= 500, y = 25;
    cout <<"Here is value x before swap: "<< x << endl;
    cout <<"Here is value y before swap: "<< y << endl;
    
    swap(x,y);
    
    cout << "Here is value x after swap: "<< x << endl;
    cout << "Here is value y after swap: "<< y << endl;
        
    return 0;
}

