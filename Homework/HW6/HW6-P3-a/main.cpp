/*
* Name: Harley Webb
* Student ID: 2511318
* Date: 10/17/15
* HW: 6
* Problem: 3
* I Harley Webb, certify this is my own work and code.
*/

// The purpose of this program is to
// calculate the amount of ice cream
// in pounds each customer will receive
// given the specific amount of ice
// cream.

#include <iostream>
#include <cstdlib>
#include <iomanip>
using namespace std;

// This function will take the amount
// of ice cream and the amount of customers
// divide them an return that value.
double function(double icec, double custom)
{
        double total=0;
         return total = icec/custom;   
}

int main(int argc, char** argv) {

    
    cout << "Enter the amount(in pounds)of ice cream you have: ";   
    
    double icec;
    cin >> icec;
    
    cout << endl<<"Enter how many customers "
            << "need ice cream: ";
            double custom;
    cin >> custom;
    
    // The for loop is to iterate
    // the amount of customers there are
    // which is also working sync with
    // the function to give each customer
    // their share of the ice cream evenly.
    for(int count=1; count<=custom;count++)
    cout << "Customer "<<count << " will receive "
            << showpoint<<setprecision(4) // the set precision is set to 
            <<function(icec, custom)<<" pounds of " // four so a more accurate
            << " ice cream. "<<endl<<endl;  // amount of pounds can be shown
    
    return 0;
}

