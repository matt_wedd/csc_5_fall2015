/*
* Name: Harley Webb
* Student ID: 2511318
* Date: 11/20/15
* HW: 8
* Problem: 6
* I Harley Webb, certify this is my own work and code.
*/

#include <iostream>
#include <cstdlib>
#include <vector>
using namespace std;

bool driverTest(vector<int>v,int num);

int main(int argc, char** argv) {

    
    int num=3;
   
    vector<int>v;
    for(int i=0;i<10;i++)
        v.push_back(3*i);
    
    if(driverTest(v,num) == true)
        cout<<"The number is in the vector."<<endl;
    else
        cout<<"The number is not in the vector."<<endl;
        
    return 0;
}

bool driverTest(vector<int>v, int num)
{
 for(int i=0;i<v.size();i++)
    {
     if(v[i] == num)
         return true; 
    }    
 return false;

}
