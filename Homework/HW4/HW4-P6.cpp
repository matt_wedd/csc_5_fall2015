//
// Name: Harley Webb
// Student ID : 2511318
// Date : 10 / 01 / 15
// HW : 4
// Problem : 6
// I Harley Webb certify this is my own work and code
//

#include "stdafx.h"
#include <iostream>
using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{


	char play_again;
	do{
		char input1, input2;
		cout << "User1 enter R for rock, P for paper, "
			<< "S for scissors: ";
		cin >> input1;
		cout << endl;
		cout << "User2 enter R for rock, P for paper, "
			<< "S for scissors: ";
		cin >> input2;
		cout << endl;
		if (input1 == 's' && input2 == 'p')
			cout << "User1 Wins " << endl;
		else if (input1 == 's' && input2 == 'P')
			cout << "User1 Wins" << endl;
		else if (input1 == 'S' && input2 == 'p')
			cout << "User1 Wins " << endl;
		else if (input1 == 'S' && input2 == 'P')
			cout << "User1 Wins" << endl;
		else if (input1 == 'P' && input2 == 'R')
			cout << "User1 Wins" << endl;
		else if (input1 == 'P' && input2 == 'r')
			cout << "User1 Wins" << endl;
		else if (input1 == 'p' && input2 == 'R')
			cout << "User1 Wins" << endl;
		else if (input1 == 'p' && input2 == 'r')
			cout << "User1 Wins" << endl;
		else if (input1 == 'R' && input2 == 's')
			cout << "User1 Wins" << endl;
		else if (input1 == 'R' && input2 == 'S')
			cout << "User1 Wins " << endl;
		else if (input1 == 'r' && input2 == 'S')
			cout << "User1 Wins" << endl;
		else if (input1 == 'r' && input2 == 's')
			cout << "User1 Wins" << endl;
		else if (input2 == 's' && input1 == 'p')
			cout << "User2 Wins " << endl;
		else if (input2 == 's' && input1 == 'P')
			cout << "User2 Wins" << endl;
		else if (input2 == 'S' && input1 == 'p')
			cout << "User2 Wins " << endl;
		else if (input2 == 'S' && input1 == 'P')
			cout << "User2 Wins" << endl;
		else if (input2 == 'P' && input1 == 'R')
			cout << "User2 Wins " << endl;
		else if (input2 == 'P' && input1 == 'r')
			cout << "User2 Wins" << endl;
		else if (input2 == 'p' && input1 == 'R')
			cout << "User2 Wins " << endl;
		else if (input2 == 'p' && input1 == 'r')
			cout << "User2 Wins" << endl;
		else if (input2 == 'R' && input1 == 's')
			cout << "User2 Wins " << endl;
		else if (input2 == 'R' && input1 == 'S')
			cout << "User2 Wins" << endl;
		else if (input2 == 'r' && input1 == 'S')
			cout << "User2 Wins " << endl;
		else if (input2 == 'r' && input1 == 's')
			cout << "User1 Wins" << endl;
		else if (input1 == 'R' && input2 == 'R')
			cout << "No body wins, it's a tie game. " << endl;
		else if (input1 == 'R' && input2 == 'r')
			cout << "No body wins, it's a tie game. " << endl;
		else if (input1 == 'r' && input2 == 'R')
			cout << "No body wins, it's a tie game. " << endl;
		else if (input1 == 'r' && input2 == 'r')
			cout << "No body wins, it's a tie game. " << endl;
		else if (input1 == 'P' && input2 == 'P')
			cout << "No body wins, it's a tie game. " << endl;
		else if (input1 == 'P' && input2 == 'p')
			cout << "No body wins, it's a tie game. " << endl;
		else if (input1 == 'p' && input2 == 'p')
			cout << "No body wins, it's a tie game. " << endl;
		else if (input1 == 'P' && input2 == 'P')
			cout << "No body wins, it's a tie game. " << endl;
		else if (input1 == 'S' && input2 == 'S')
			cout << "No body wins, it's a tie game. " << endl;
		else if (input1 == 'S' && input2 == 's')
			cout << "No body wins, it's a tie game. " << endl;
		else if (input1 == 's' && input2 == 'S')
			cout << "No body wins, it's a tie game. " << endl;
		else if (input1 == 's' && input2 == 's')
			cout << "No body wins, it's a tie game. " << endl;

		cout << "If you would like to play again, enter y for yes "
			<< " or else enter n for no : ";
		cin >> play_again;

	} while (play_again == 'Y' || play_again == 'y');

		
	
	return 0;
}

