/*
* Name: Harley Webb
* Student ID: 2511318
* Date: 10/17/15
* HW: 6
* Problem: 2
* I Harley Webb, certify this is my own work and code.
*/


#include <iostream>
#include <cstdlib>
#include <iomanip>
using namespace std;

// This function will take the ice cream
// amount and divide it by the amount of
// customers
double function(double icec, double custom)
{
    for (int count=0; count < custom;count++)
    {
        double total=0;
         return total = icec/custom;
    }
}

int main(int argc, char** argv) {

    cout << "Enter the amount(in pounds)of ice cream you have: ";
       
    double icec;
    cin >> icec;
    cout << endl<<"Enter how many customers "
            << "need ice cream: ";
            double custom;
    cin >> custom;
    
    for(int count=1; count<=custom;count++)
    cout << "Customer "<<count << " will receive "
            << showpoint<<setprecision(4)
            <<function(icec, custom)<<" pounds of "
            << " ice cream. "<<endl<<endl;
    
    return 0;
}

