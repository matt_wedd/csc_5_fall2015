/*
* Name: Harley Webb
* Student ID: 2511318
* Date: 12/05/15
* HW: 9
* Problem: 2
* I Harley Webb, certify this is my own work and code.
*/
#include <iostream>
#include <cstdlib>
#include <stdlib.h>
using namespace std;

int* mArray(int);
int* sArray(int);
void oArray(int, int *p);

int main(int argc, char** argv) {

    int *p;
    cout <<"Enter an array size:";
    int size;
    cin >>size;
    
    p = mArray(size);
    p = sArray(size);
    oArray(size, p);
    return 0;
}

int* mArray(int size)
{
    int *p;
 p = new int[size];
 return p;
}

int* sArray(int size)
{
    int *p = new int[size];
    srand(time(0));
    for(int i=0;i<size;i++)
        p[i]=rand()%6+1;
    return p;
}

void oArray(int size, int* p)
{
    for(int i=0;i<size;i++)
        cout<<p[i]<<" ";
}
