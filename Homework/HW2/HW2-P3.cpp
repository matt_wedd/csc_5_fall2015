/*
* Name: Harley Webb
* Student ID: 2511318
* Date: 9/18/15
* HW: 2
* Problem: 3
* I Harley Webb certify this is my own work and code
*/

#include "stdafx.h"
#include <iostream>
#include <iomanip>
using namespace std;

int _tmain(int argc, _TCHAR* argv[])

{
	float Q1;
	double Q2;
	float Q3;
	float Q4;

	Q1 = 10 + 9 + 8 ;
	Q2 = 7 + 8 + 7;
	Q3 = 10 + 10 + 9 ;
	Q4 = 9 + 9 + 10 ;

	cout << " name        Quiz 1   Quiz 2   Quiz 3   Quiz 4\n";
	cout << " ____        ______   ______   ______   ______\n";
	cout << " John          10         7      10        9\n";
	cout << " Mary           9         8      10        9\n";
	cout << " Matthew        8         7       9       10\n\n";

	cout << " Average     "<< right << showpoint <<setprecision(3) << Q1/3 << "      " << Q2/3;
	cout << "    " << Q3/3 << "     " << Q4/3 << "\n\n" ;
	return 0;

}

