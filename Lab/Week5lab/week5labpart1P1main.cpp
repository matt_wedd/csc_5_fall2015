// ConsoleApplication13.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	char y_or_n;
	do{
		int bal_due = 0, tot_amo = 0, min_pay = 0, fir_thou = 1000;
		cout << "Enter your balance amount: ";
		cin >> bal_due;

		if (bal_due <= fir_thou)
		{
			double int_bal = 0, lar_ten = 100, int_rate = 0.015;
			int_bal = bal_due * int_rate;
			tot_amo = int_bal + bal_due;
			cout << setprecision(2) << fixed << showpoint
				<< "total amount is $" << tot_amo << endl;
			if (tot_amo > lar_ten)
			{
				double new_tot, ten_per = 0.1;
				new_tot = tot_amo * ten_per;
				cout << "mimimum payment is $" << new_tot << endl;
			}
			else if (tot_amo < lar_ten)
				cout << "Your minimum payment is $10";

		}
		else if (bal_due > fir_thou)
		{
			double int_bal, lar_ten = 0.1, int_rate = 0.01;

			int_bal = bal_due * int_rate;
			tot_amo = int_bal + bal_due;
			cout << setprecision(2) << fixed << showpoint <<
				"Your total is $" << tot_amo << endl;
			// No need for if statments, his/her's balance is over a thousand

			tot_amo = tot_amo * lar_ten;

			cout << "Your minimum payment is " << tot_amo << endl;

		}


		cout << "would you like to repeat calculations? enter Y or N \n";

		cin >> y_or_n;
	} while (y_or_n == 'Y' || y_or_n == 'y');

	return 0;
}

