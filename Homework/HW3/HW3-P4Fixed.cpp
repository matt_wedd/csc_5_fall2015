//
// Name: Harley Webb
// Student ID : 2511318
// Date : 9 / 28 / 15
// HW : 3
// Problem : 4
// I Harley Webb certify this is my own work and code
//

#include "stdafx.h"
#include <iostream>
using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	int posNum = 0, negNum = 0, allNum = 0;// counter = 0;

	for (int counter = 0; counter < 10; counter++)
	{
		cout << "Enter a number: " <<endl;
			int Num;
			cin >> Num;
			if (Num<0){
				negNum = negNum + Num;
			}

			if (Num>0){
				posNum = posNum + Num;
			}

	}
	allNum = negNum + posNum;

	cout << "Positive Numbers " << posNum << endl;
	cout << "Negative Numbers " << negNum << endl;
	cout << "Total of all Numbers " << allNum << endl;

	return 0;
}

