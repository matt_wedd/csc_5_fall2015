/*
* Name: Harley Webb
* Student ID: 2511318
* Date: 10/09/15
* HW: 5
* Problem: 9
* I Harley Webb, certify this is my own work and code.
*/

#include "stdafx.h"
#include <iostream>
#include <fstream>
using namespace std;

const double lit_per_gal = 0.264179;

double function(double miles1, double liters2)
{

	return miles1 / (lit_per_gal*liters2);
}




int _tmain(int argc, _TCHAR* argv[])
{

	ifstream inFile;
	double miles, liters, miles2, liters2;

	inFile.open("data1.dat");
	inFile >> miles;
	inFile >> liters;
	inFile.close();

	inFile.open("data2.dat");
	inFile >> miles2;
	inFile >> liters2;
	inFile.close();

	cout << "How many liters of gas did car1"
		<< " consume? " << liters << endl;


	cout << "How many miles did car1 drive? "
		<< miles << endl;

	cout << "Car1 runs " << function(miles, liters)
		<< " miles per gallon." << endl<<endl;

	cout << "How many liters of gas did car2"
		<< " consume? " << liters2 << endl;


	cout << "How many miles did car2 drive? "
		<< miles2 << endl;

	cout << "Car2 runs " << function(miles2, liters2)
		<< " miles per gallon." << endl<<endl;
	double total1, total2;

	total2 = function(miles2, liters2);

	total1 = function(miles, liters);

	if (total1 > total2)
		cout << "Car1 has better fuel efficiency. " << endl;
	else if (total1 < total2)
		cout << "Car2 has better fuel efficiency. "<<endl;



	return 0;
}

