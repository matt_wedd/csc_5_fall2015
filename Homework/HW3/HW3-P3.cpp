//
// Name: Harley Webb
// Student ID : 2511318
// Date : 9 / 25 / 15
// HW : 3
// Problem : 3
// I Harley Webb certify this is my own work and code
//
#include "stdafx.h"
#include <iostream>
#include <iomanip>
using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{
	cout << "How many people will be attending your meeting? ";
	int roomval;
	cin >> roomval;
	cout << "\n";

	if (roomval <= 40)
	{
		roomval = 40 - roomval;
		cout << "Your meeting fits fire regulation laws, and is allow "
			<< roomval << " more to attend. \n";
	}
	else if (roomval > 40)
	{
		roomval = roomval - 40;
		cout << "Your meeting violates fire" 
			<< "regulation laws and is over capacity by "
			<< roomval << " more people, your meeting cannot be held.\n\n ";
	}

	return 0;
}

