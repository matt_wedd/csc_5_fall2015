/*
* Name: Harley Webb
* Student ID: 2511318
* Date: 10/17/15
* HW: 6
* Problem: 9
* I Harley Webb, certify this is my own work and code.
*/

#include <cstdlib>
#include <iomanip>
#include <string>
#include <iostream>
using namespace std;

int count_coin(int &quart,int &dimes,int &pennies )
{
    int add=0,sub=0;
   // Change names to Professors names
    sub = quart;
    add = sub;
    quart= quart/25;
    if ( quart==0)
    {
        dimes= sub/10;
        if(dimes == 0)
            pennies = add;
        else if(dimes ==1)
            pennies= add-10;
        else if(dimes ==2)
            pennies=add-20;
        }
    else if(quart ==1)
    {
         dimes= (sub-25)/10;  
         add= sub-25;
        if(dimes == 0)
            pennies=add;
        else if(dimes ==1)
            pennies=add-10;
        else if(dimes ==2)
            pennies=add-20;
        
    }
    else if(quart == 2)
    {
        dimes=(sub-50)/10;
        add=add-50;
        if(dimes == 0)
            pennies=add;
        else if(dimes ==1)
            pennies=add-10;
        else if(dimes ==2)
            pennies=add-20;
        
    }
    else if(quart==3)
    {
        dimes=(sub-75)/10; 
        add=add-75;
        if(dimes == 0)
            pennies=add;
        else if(dimes ==1)
            pennies=add-10;
        else if(dimes ==2)
            pennies=add-20;
        
    }        
         
            
   return quart, dimes, pennies; 
}

int main(int argc, char** argv) {
    int quarter=0,dime=0, penny=0,
            origin=0, coin_value=0;
    char choice;
    do{
    cout << "Input the amount of change you have: ";
    cin >> coin_value;
    origin=origin+coin_value;
    cout <<endl;
    if ( coin_value > 100 || coin_value <0)
        cout <<"Invalid amount";
    else
    {
        count_coin(coin_value,dime,penny);
        quarter=coin_value;
        cout << origin<< " cents can be seen as"<<endl;
        cout <<quarter<<" quarter(s)"
               <<" "<<dime<<" Dime(s)"
                <<" and "<< penny<<" Penny(s)."<<endl;
    }
    cout << "If you would like to try again enter"
         <<" y for yes or else n for no: ";
            cin >> choice;
            cout <<endl;
    }while(choice!='n' || choice!='n');
    return 0;
}

