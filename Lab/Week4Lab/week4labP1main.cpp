
#include <cstdlib>
#include <iostream>
using namespace std;

int main(int argc, char** argv) {

    // Prompt the user
    cout << "Please enter your test score ";
    int testScore;
    
    // Get input user
    cin >> testScore;
    
    if(testScore <= 100 && testScore > 89)
    {
        cout << "You got an A!";
        cout << "Keep it up!";
    }
    else if (testScore < 90 && testScore > 79)
    {
        cout << "You got a B! ";
    }
    else if (testScore < 80 && testScore > 69)
    { 
        cout << "You got a C!";
    }
    else if (testScore < 70 && testScore > 59)
    {
        cout << "You got a D!";
    }
    else if (testScore < 60 && testScore >= 0)
    {
        cout << "You got an F!";
    }
    else
    { cout << "Enter another number";
    }
    return 0;
}

