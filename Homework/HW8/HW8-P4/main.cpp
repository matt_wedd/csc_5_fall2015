/*
* Name: Harley Webb
* Student ID: 2511318
* Date: 11/15/15
* HW: 8
* Problem: 4
* I Harley Webb, certify this is my own work and code.
*/

#include <iostream>
#include <cstdlib>
#include <vector>
using namespace std;

vector<int> vect(vector<int> &v ,int num);

int main(int argc, char** argv) {

    cout <<"(0,2,4,6,8,10,12,14,16,18)";
    cout <<endl<<endl;
    cout <<"Choose a number from this vector to be deleted:";
    int num=0, count=0;
    cin >> num;
    
    vector<int> v;
    for(int i=0;i<10;i++)
    {
        v.push_back(2*i);
        if(num != i*2)
            count++;
    }
    if(num != 10 && num%2 ==0 && num <= 18 && num >= 0)
    {
        vect(v,num);
    }
    else
        cout<<"Invalid input"<<endl;
    
    return 0;
}

vector<int> vect(vector<int> &v, int num)
{
    for(int i=0;i<v.size();i++)
    {
        if(v[i] == num)
            swap(v[i],v[i+1]);
    }
    v.pop_back();
    return v;
}