/*
* Name: Harley Webb
* Student ID: 2511318
* Date: 12/06/15
* HW: 9
* Problem: 11
* I Harley Webb, certify this is my own work and code.
*/
#include <iostream>
#include <cstdlib>

using namespace std;

int main(int argc, char** argv) {

    int var1 =42;
    int var2 = var1;
    
    int* p1 = &var1;
    int* p2 = &var2;
    
    cout <<"v:"<<var1<<endl;
    cout <<"v1:"<<var2<<endl<<endl;
    
    var1 = 12;
    cout <<"v:"<<var1<<endl;
    cout<<"v1:"<<var2<<endl<<endl;
    
    *p1 = 25;
    
    cout <<"v:"<<var1<<endl;
    cout <<"v1:"<<var2<<endl<<endl;
   
    p2 = &var1;
    *p2 = 60;
    cout<<"v:"<<var1<<endl;
    cout<<"v1:"<<var2<<endl;
    
    return 0;
}

