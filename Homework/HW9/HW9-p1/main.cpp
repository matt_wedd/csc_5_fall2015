/*
* Name: Harley Webb
* Student ID: 2511318
* Date: 12/05/15
* HW: 9
* Problem: 1
* I Harley Webb, certify this is my own work and code.
*/
#include <cstdlib>
#include <iostream>
#include <stdlib.h>
using namespace std;

int main(int argc, char** argv) {

    srand(time(0));
    cout <<"Enter the size of array you would like:";
    int size=0;
    cin >> size;
    
    int* p = new int[size];
    
    for(int i=0;i < size;i++)
       p[i] = rand()%21-1;
    
    for(int i=0;i < size;i++)
      cout<< p[i]<<" "; 
    cout <<endl;
    
    return 0;
    
}

