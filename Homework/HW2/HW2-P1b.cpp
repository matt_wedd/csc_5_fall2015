/*
* Name: Harley Webb
* Student ID: 2511318
* Date: 9/18/15
* HW: 2
* Problem: 1b
* I Harley Webb certify this is my own work and code
*/

#include "stdafx.h"
#include <iostream>
using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{


	int x;
	int y;

	cout << "Give a value to x \n";
	cin >> x;

	cout << "Give a value to y \n";
	cin >> y;

	// This is a logical error because the purpose of this program is to add number x to number y, instead the program just adds x plus x

	cout << x << " + " << y << "=" << x + x;


	return 0;
}

