/*
* Name: Harley Webb
* Student ID: 2511318
* Date: 12/08/15
* HW: 10
* Problem: 6
* I Harley Webb, certify this is my own work and code.
*/
#include <cstdlib>
#include <iostream>
using namespace std;

class Time
{
private:
    int second;
    int minutes;
    int hours;
public:
    int getSeconds();
    int getMinutes();
    int getHours();
    
    void setSeconds(int);
    void setMinutes(int);
    void setHours(int);

};

int Time::getSeconds()
{
    return second;
}
int Time::getMinutes()
{
    return minutes;
}
int Time::getHours()
{
    return hours;
}

void Time::setSeconds(int s)
{
    second=s;
}
void Time::setMinutes(int m)
{
    minutes= m;
}
void Time::setHours(int h)
{
    hours=h;
}

int main(int argc, char** argv) {

    Time t;
   
    t.setSeconds(60);
    t.setMinutes(30);
    t.setHours(1);
    cout <<"Hours"<<t.getHours();
    cout<< " minutes"<<t.getMinutes();
    cout <<" seconds"<<t.getSeconds();
    cout<<endl;
    
    
    
    return 0;
}

