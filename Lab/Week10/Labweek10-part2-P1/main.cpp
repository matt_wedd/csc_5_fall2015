
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <stdlib.h>
#include <iomanip>
#include <vector>
using namespace std;


int main(int argc, char** argv) {

    ifstream infile;
    cout << "Enter a file name and the integers will be displayed:";
    string file;
    cin >>file;
    vector<int>v;
    int num;
    infile.open(file.c_str());
    
    if(infile)
    {
        while( infile >> num)
        v.push_back(num);
        
        infile.close();
        for (int i=0;i<v.size();i++)
            cout << v[i]<<" ";
    }
    else
        cout <<"Invalid file."<<endl;
    
    
    return 0;
}

