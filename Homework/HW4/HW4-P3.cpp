//
// Name: Harley Webb
// Student ID : 2511318
// Date : 9 / 30 / 15
// HW : 4
// Problem : 3
// I Harley Webb certify this is my own work and code
//


#include "stdafx.h"
#include <iostream>
#include <iomanip>
using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{

	int high_inch, weigh_poun, age_year, BM_R
		, choc_carl = 230;
	char try_agan;
	do{
		cout << "Enter M for male or W for female: ";
		char man_wom;
		cin >> man_wom;
		if (man_wom == 'M' || man_wom == 'm')
		{
			cout << "Enter your height in inches: ";
			cin >> high_inch;
			cout << "Enter your weight in pounds: ";
			cin >> weigh_poun;
			cout << "Enter your age in years: ";
			cin >> age_year;

			BM_R = 66 + (6.3 * weigh_poun) + (12.9 * high_inch)
				- (6.8 *age_year);
			cout << "You should consume " << BM_R / choc_carl
				<< " chocolate bars to maintain "
				<< "your weight. \n" << endl;
			cout << "If you would like to start over enter "
				<< " Y for yes or N for no: ";
			cin >> try_agan;
		}
		else if (man_wom == 'W' || man_wom == 'w')
		{
			cout << "Enter your height in inches: ";
			cin >> high_inch;
			cout << "Enter your weight in pounds: ";
			cin >> weigh_poun;
			cout << "Enter your age in years: ";
			cin >> age_year;
			BM_R = 655 + (4.3 * weigh_poun) + (4.7 * high_inch)
				- (4.7 *age_year);
			cout << "You should consume " << BM_R / choc_carl
				<< " chocolate bars to maintain "
				<< "your weight. \n" << endl;
			cout << "If you would like to start over enter "
				<< " Y for yes or N for no:";
			cin >> try_agan;

		}
	} while (try_agan == 'y' || try_agan == 'Y');
	return 0;
}

