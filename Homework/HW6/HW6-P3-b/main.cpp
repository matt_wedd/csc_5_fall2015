/*
* Name: Harley Webb
* Student ID: 2511318
* Date: 10/17/15
* HW: 6
* Problem: 3
* I Harley Webb, certify this is my own work and code.
*/

#include <cstdlib>
#include <iomanip>
#include <iostream>
using namespace std;

// Function prototypes.
double max(double, double);
int max(int, int);

int main(int argc, char** argv) 
{
  double x = 20;
  double y=5;
  cout<< "Here is the bigger number "<<
          max(x,y)<<endl<<endl;
          return 0;
}

// This function is set with
// the data type double
// and will return any double
double max(double x, double y)
{
    if(x>y)
        return x;
    else
    return y;
}

// This function is set
// with the data type
// int which will return
// any integer.
int max(int x, int y)
{
    if(y>x)
        return y;
    else
    return x;
}