/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 17, 2015, 10:51 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;


int main(int argc, char** argv) {

    // Do not pass 80 character mark.
    // Avoid line wrapping
    
    // Guessing game
    // Generate a random number
    srand(time(0)); // Seed
    int num = rand(); // Get random number
    num %= 100; // Get a number between 0-99
    
    // Prompt user
    cout << "Please enter a number between 0-99: ";
    
    int userNum;
    cin >> userNum;
    
    if ( userNum == num)
    {
        cout << "Correct!" << endl;
    }
    else 
    {
        cout << "Wrong!" << endl;
    }
        
        return 0;
}

